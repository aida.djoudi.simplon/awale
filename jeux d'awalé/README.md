# Awalé
===façon de travailler

- groupe sur gitlab
- dépôt pour le groupe
-une branche par developpeur
- on commit le plus possible après chaque modification fonctionelle
- les conflits sont géré entre  qui ont écrit le code en conflit
- quand la transition sera finie, on mergera tous dans master (puis on mergera master dans nos branches respectives)
- fichiers html déstinés à être visible sur le site dans le dossier vues
- fichiers CSS et dérivés dans le dossier style/
- fichiers images/vidéos dans le dossier medias/
- javascript dans js/
- on utilise bootstrap 

===les règle de jeux

Règle 1 : But du jeu Le but du jeu est de s'emparer d'un maximum de graines. Le joueur qui a le plus de graines à la fin de la partie l'emporte.

Règle 2 : Le terrain de jeu Le terrain de jeu est divisé en deux territoires de 6 trous chacun. Votre territoire est en bas, c'est le territoire Sud; celui de votre adversaire est au dessus, c'est le territoire Nord. Au départ dans les douze trous sont réparties 48 graines (4 par trou).

Règle 3 : Le tour de jeu. Chaque joueur joue à son tour, celui qui joue en premier est tiré au hasard. Le joueur va prendre l'ensemble des graines présentes dans l'un des trous de son territoire et les distribuer, une par trou, dans le sens inverse des aiguilles d'une montre.

Règle 4 : Capture Si la dernière graine semée tombe dans un trou de l'adversaire comportant déjà 1 ou 2 graines, le joueur capture les 2 ou 3 graines résultantes. Les graines capturées sont sorties du jeu. (Le trou est alors laissé vide)

Règle 5: Capture multiple Lorsqu'un joueur s'empare de deux ou trois graines, si la case précédente contient également deux ou trois graines, elle sont capturées aussi, et ainsi de suite.

Règle 6: Bouclage Si le nombre de graines prises dans le trou de départ est supérieur à 11, cela fait que l'on va boucler un tour : auquel cas, à chaque passage, la case de départ est sautée et donc toujours laissée vide. Un trou contenant assez de graines pour faire une boucle complète s'appelle un Krou.

Règle 7: Donner à manger On n'a pas le droit d'affamer l'adversaire : De même, un joueur n'a pas le droit de jouer un coup qui prenne toutes les graines du camp de l'adversaire.









