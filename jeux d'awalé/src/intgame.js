///////////////////////////////////////initialisation of game//////////////////////////////////////////

//------------------------------------------player1---------------------------------------------------

//board total seed player 1
let p1Totalvues = document.querySelector("#p1Totalvues");
let p1Totalseeds = 0

//---------------------------------------------- player2---------------------------------------------

//board total seed player 2
let p2Totalvues = document.querySelector("#p2Totalvues");
let p2Totalseeds = 0

//---------------------------------------the boards games-------------------------------------------

// board seeds
var slotsSeeds = [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4];

//tables players
let buttons = []

for (var i = 0; i < 12; i++) {
    buttons.push(document.getElementById("Btn" + i))
};

// function vues dom
function vuesdom() {
    p2Totalvues.innerHTML = p2Totalseeds;
    p1Totalvues.innerHTML = p1Totalseeds;
    for (var i = 0; i < 12; i++) { buttons[i].innerHTML = slotsSeeds[i] }
}
// the player who plays
let thePlayerwhoplays = 0

//hand storage
let handStorage = 0

// button start game
let startGame = document.querySelector("#startGame")

// button reset game
let reset = document.querySelector("#reset")

let randomPlayer = ["player1", "player2"]

///////////////////////////////////////////////GAMES//////////////////////////////////////////////////

//--------------------------------------------Int games--------------------------------------------

startGame.addEventListener("click", function newgame() {
    thePlayerwhoplays = randomPlayer[Math.floor(Math.random() * randomPlayer.length)]
    startGame.disabled = true;
    reset.disabled = false;
    if (thePlayerwhoplays == "player1") {
        alert("le joueur 1 commence la partie, bon jeu a vous !");
    }
    if (thePlayerwhoplays == "player2") {
        alert("le joueur 2 commence la partie, bon jeu a vous !");
    }
    switchPlayer(thePlayerwhoplays)
});

//--------------------------------------------reset games--------------------------------------------

reset.addEventListener("click", function resetgame() {
    var confirme = confirm("vous etes de vouloire recommencer la partie ?")
    if (confirme == true) {
        document.location.reload(true);
    }
})

//---------------------------------------------choice of slots--------------------------------------

for (let i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener("click", (e) => {
        var whereTakeslot = buttons.indexOf(e.target);
        if (slotsSeeds[whereTakeslot] == 0) {
            alert("Il n'y a pas de grains ici !!!")
            return
        }
        distribute(whereTakeslot);
    });
};


//-------------------------------------------take and distribute-----------------------------------

function distribute(whereTakeslot) {

    var takeSeeds = slotsSeeds[whereTakeslot];
    slotsSeeds[whereTakeslot] = 0;
    var seedsDrop = whereTakeslot + 1
    while (takeSeeds != 0) {
        if (seedsDrop >= 12) {
            seedsDrop = 0
        };
        if (whereTakeslot === seedsDrop) {
            seedsDrop = seedsDrop + 1;
        };
        slotsSeeds[seedsDrop] = slotsSeeds[seedsDrop] + 1;
        takeSeeds = takeSeeds - 1
        seedsDrop = seedsDrop + 1
    };

    checkingSeedtake(seedsDrop)
};

//-----------------------------------------checking Seed taking--------------------------------------

function checkingSeedtake(seedsDrop) {

    let locationLastdropslot = seedsDrop - 1
    conditionOftake(locationLastdropslot)
    switchPlayer(handStorage);
}

//------------------------------------------condition Of taking-----------------------------------------

function conditionOftake(locationLastdropslot) {

    if ((thePlayerwhoplays === 'player1') && (locationLastdropslot >= 6) && (locationLastdropslot <= 11)) {
        for (i = 6; i <= slotsSeeds[11]; i++) {
            if ((slotsSeeds[locationLastdropslot] === 2) || (slotsSeeds[locationLastdropslot] === 3)) {
                handStorage = handStorage + slotsSeeds[locationLastdropslot]
                slotsSeeds[locationLastdropslot] = 0
                locationLastdropslot = locationLastdropslot - 1
            }

        }
    }

    if ((thePlayerwhoplays === 'player2') && (locationLastdropslot >= 0) && (locationLastdropslot <= 5)) {
        for (i = 0; i <= slotsSeeds[5]; i++) {
            if ((slotsSeeds[locationLastdropslot] === 2) || (slotsSeeds[locationLastdropslot] === 3)) {
                handStorage = handStorage + slotsSeeds[locationLastdropslot]
                slotsSeeds[locationLastdropslot] = 0
                locationLastdropslot = locationLastdropslot - 1
            }
        }

    }
}

//---------------------------------------inspect total seeds---------------------------------------------
function inspectTotalseeds() {

    if (p1Totalseeds >= 24) {
        alert("Bravo le joueur 1 a Gagné(e) !!!!! tu as " + p1Totalseeds + "graines.");
        document.location.reload(true);
    }

    if (p2Totalseeds >= 24) {
        alert("Bravo le joueur 2 a Gagné(e) !!!!! tu as " + p2Totalseeds + "graines.");
        document.location.reload(true);
    }
};

//---------------------------------------copmparative total seeds---------------------------------------------

function copmparativeTotalseeds() {
    if (p1Totalseeds > p2Totalseeds) {
        alert("Bravo le joueur 1 a Gagné(e) !!!!! tu as " + p1Totalseeds + "graines.");
        document.location.reload(true);

    }
    if (p2Totalseeds > p1Totalseeds) {

        alert("Bravo le joueur 2 a Gagné(e) !!!!! tu as " + p2Totalseeds + "graines.");
        document.location.reload(true);
    }
    if ((p2Totalseeds == p1Totalseeds) && (p1Totalseeds != 0)) {

        alert("egaliter vous avez le même nombre de graine !!!!");
        document.location.reload(true);
    }
};

//------------------------------------------disabled Btn---------------------------------------------
//disabled Btn P1

function disabledBtnP1() { for (i = 0; i <= 5; i++) { buttons[i].disabled = true; }; for (i = 6; i <= 11; i++) { buttons[i].disabled = false; }; };

//disabled Btn P2

function disabledBtnP2() {
    for (i = 0; i <= 5; i++) {
        buttons[i].disabled = false;
    };
    for (i = 6; i <= 11; i++) {
        buttons[i].disabled = true;
    };

};

//------------------------------------slice and every slot seed--------------------------------------------

function p1sliceEveryslotseed() {
    var p1slotsseeds = slotsSeeds.slice(6)
    const verif0seed = (seedValue) => seedValue == 0;
    var result1 = p1slotsseeds.every(verif0seed);
    if (result1 == true) {
        exit2Gameslotseed()
    }
};

function p2sliceEveryslotseed() {
    var p2slotsseeds = slotsSeeds.slice(0, 6)
    const verif0seed = (seedValue) => seedValue == 0;
    var result2 = p2slotsseeds.every(verif0seed);
    if (result2 = true) {
        exit1Gameslotseed()
    };
}


//------------------------------------exit game slot seed--------------------------------------------

function exit1Gameslotseed() {

    if ((slotsSeeds[5] <= 0) && (slotsSeeds[4] <= 1) &&
        (slotsSeeds[3] <= 2) && (slotsSeeds[2] <= 3) &&
        (slotsSeeds[1] <= 4) && (slotsSeeds[0] <= 5)) {
        copmparativeTotalseeds()
    } else {

    }
};

function exit2Gameslotseed() {
    if ((slotsSeeds[11] <= 0) && (slotsSeeds[10] <= 1) &&
        (slotsSeeds[9] <= 2) && (slotsSeeds[8] <= 3) &&
        (slotsSeeds[7] <= 4) && (slotsSeeds[6] <= 5)) {
        copmparativeTotalseeds()
    }
}

//-------------------------------------------switch the Player-----------------------------------------

function switchPlayer() {
    switch (thePlayerwhoplays) {

        case 'player1':
            document.getElementById("ligneP1").style.backgroundColor = 'rgba(2, 13, 168, 0.76)';
            document.getElementById("ligneP2").style.backgroundColor = 'rgba(228, 15, 15, 0.76)';;
            document.getElementById("ratingplayerP1").style.backgroundColor = 'rgba(2, 13, 168, 0.76)';
            document.getElementById("ratingplayerP2").style.backgroundColor = 'rgba(228, 15, 15, 0.76)';
            p2Totalseeds = p2Totalseeds + handStorage;
            handStorage = 0
            vuesdom();
            disabledBtnP2();
            inspectTotalseeds();
            p1sliceEveryslotseed();
            thePlayerwhoplays = 'player2';
            break;

        case 'player2':
            document.getElementById("ligneP1").style.backgroundColor = 'rgba(228, 15, 15, 0.76)';
            document.getElementById("ligneP2").style.backgroundColor = 'rgba(2, 13, 168, 0.76)';
            document.getElementById("ratingplayerP1").style.backgroundColor = 'rgba(228, 15, 15, 0.76)';
            document.getElementById("ratingplayerP2").style.backgroundColor = 'rgba(2, 13, 168, 0.76)';
            p1Totalseeds = p1Totalseeds + handStorage;
            handStorage = 0;
            vuesdom();
            disabledBtnP1();
            inspectTotalseeds();
            p2sliceEveryslotseed();
            thePlayerwhoplays = 'player1';
    }
};
/////////////////////////////////////////////////////////teste//////////////////////////////////////////////////////////////